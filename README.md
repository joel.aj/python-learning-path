# Python Learning Path

## Command Line Basics

**Session** on CLI.

[Command Line Crash Course](https://learnpythonthehardway.org/book/appendixa.html)

## Version Control Systems

**Session** on Git and Github

[How to use Git and Github](https://in.udacity.com/course/how-to-use-git-and-github--ud775)

## HTML and CSS

[Introductory Course](https://in.udacity.com/course/intro-to-html-and-css--ud304)

### Projects:

1. Themed Website

	Build your own themed website. Use design elements from [http://themes.rokaux.com/unishop/v1.4/index.html](http://themes.rokaux.com/unishop/v1.4/index.html)

	Submit the URL to the GIt repository and the URL where the project is deployed. Please name the repository theme-(name) e.g. (theme-john)
2. Responsive Portfolio Website

	Build two versions of the Udacity portfolio website. With and without the Bootstrap CSS framework. These can be two different files in the same repository. You're required to submit the following:

	Link to repository. The repository to be named portfolio-(name) example portfolio-john

	Live URL containing links to both versions of the portfolio.


	In an index.html provide links to both the files.
    * portfolio.html
	* portfolio-bootstrap.html

## Web Architecture Overview

**Session** on Web Architecture Overview

## Python

**Session** on environment setup - virtualenv, pip and packaging.

[Introduction to Python](https://www.youtube.com/watch?v=HBxCHonP6Ro&list=PL6gx4Cwl9DGAcbMi1sH6oAMk4JHw91mC_)

Go through [PEP-8 Style Guide](https://www.python.org/dev/peps/pep-0008/)

## Data Project - Round 1

Read the CSV, slice and dice and plot with matplotlib.

## Django

[Introduction to Django](https://www.youtube.com/playlist?list=PL6gx4Cwl9DGBlmzzFcLgDhKTTfNLfX1IK)

## Data Project - Round 2

Use Django as backend and JSON as data store to implement the project.

## SQL

[Introduction to Relational Databases](https://in.udacity.com/course/intro-to-relational-databases--ud197)

## Data Project - Round 3

Use Django as backend and MySQL as database.

## JavaScript and Ecosytem

**Session** on JavaScript Environment Setup - JS, Node, NPM. Introduction to JavaScript.

[Introduction to JavaScript](https://in.udacity.com/course/intro-to-javascript--ud803)

**Session**: Recap on Functions, Scope and Callbacks.

[Introduction to Jquery](https://in.udacity.com/course/intro-to-jquery--ud245)

### NavTree Project in Jquery

Repository name: navtree-(name) Example navtree-john


Build a hierarchical, navigable tree structure using jQuery. The tree structure should be at least 3 levels deep.

For example:

Depth 0

-- Depth 1

-- Depth 2

It should expand and minimize to reveal and hide nested nodes. Do not use an existing library to build the tree structure.

Please use the IPL dataset that you had previously used for the Visualisation assignment. An example hierarchy could be

Year > Team > Players

When the last entity is selected, show a stat relevant to that identity on the page. This can either be a visualisation or a table.

--

**Session**: Recap Jquery.

**Session**: JS Utility functions.

**Session**: Object and Prototypes.

[Introduction to Object Oriented JavaScript](https://in.udacity.com/course/object-oriented-javascript--ud015) - Only complete scopes, closures, 'this' keyword and prototype chains.

[Introduction to ES6](https://www.udacity.com/course/es6-javascript-improved--ud356)

## REST

Introduction to Django REST framework. TODO - Add Video.

## Data Project - Round 3

Use Django REST Framework and Jquery for the data project.

## React

[Build Tools: Webpack and Babel](https://stanko.github.io/webpack-babel-react-revisited/)

[Introduction to React](https://egghead.io/courses/start-learning-react)

**Session** Recap React

### NavTree Project in React

Implement the NavTree structure using the React library.

Name the project navtree-react-(name)

## Data Project - Round 4

Use React for the frontend.

## Redux

[Introduction to Redux](https://egghead.io/courses/getting-started-with-redux)

[Introduction to Idiomatic Redux](https://egghead.io/courses/building-react-applications-with-idiomatic-redux)

**Session** Recap Redux

## Data Project - Round 5

Use React and Redux for the frontend.

## Capstone Project

The final project of the bootcamp.

Thanks.









